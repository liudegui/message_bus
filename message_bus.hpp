#ifndef MESSAGE_BUS_H
#define MESSAGE_BUS_H

#include <algorithm>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstdint>
#include <functional>
#include <map>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>
#ifdef __linux__
#include <pthread.h>
#endif

namespace MessageBusSystem {

// 消息回调类型
using MessageCallback = std::function<void(const std::vector<std::uint8_t>& messageContent, std::int32_t additionalData)>;
using TimeoutCallback = std::function<void()>;

// 消息回调类型
enum class SubscriptionType { ALWAYS_SUBSCRIBE = 0, ONCE_SUBSCRIBE };

// 订阅项结构体
struct SubscriptionItem {
  MessageCallback messageCallback = nullptr;                               // 消息回调函数
  TimeoutCallback timeoutCallback = nullptr;                               // 超时回调函数
  std::int32_t timeoutIntervalMilliseconds = 1000;                         // 超时时间间隔，单位：毫秒
  std::int64_t timeoutTimestampMicroseconds = 0;                           // 超时戳，单位：微秒
  std::vector<std::int32_t> subscribedMessageIds;                          // 订阅的消息ID
  SubscriptionType subscriptionType = SubscriptionType::ALWAYS_SUBSCRIBE;  // 订阅类型
};

// 定时器默认间隔（单位：毫秒）
constexpr std::int32_t DEFAULT_TIMER_INTERVAL_MS = 100;

class MessageBus {
 public:
  static MessageBus& instance() {
    static MessageBus instance;
    return instance;
  }

  void publishMessage(std::int32_t messageId, const std::vector<std::uint8_t>& messageContent, std::int32_t additionalData = 0);
  void checkAndHandleTimeouts();
  bool subscribeToMessage(const SubscriptionItem& item);
  void clearAllSubscriptions();
  void stop();
  void start();

 private:
  // 定时任务调度器
  class PeriodicTaskScheduler {
   public:
    PeriodicTaskScheduler() : stopped_(true), tryToStop_(false) {
    }

    // 禁止复制构造函数和复制赋值操作符
    PeriodicTaskScheduler(const PeriodicTaskScheduler& t) = delete;
    PeriodicTaskScheduler& operator=(const PeriodicTaskScheduler& t) = delete;

    ~PeriodicTaskScheduler() {
      stop();
    }

    void startTask(std::int32_t intervalMs, const std::function<void()>& task) {
      if (!stopped_) {
        return;
      }

      stopped_ = false;
      std::thread([this, intervalMs, task]() {
#ifdef __linux__
        // 设置线程的名称
        constexpr char* threadName = "PeriodicTaskThread";
        pthread_setname_np(pthread_self(), threadName);  // 设置 Linux 上线程名称
        // 设置线程的策略和优先级为 FIFO，高优先级
        struct sched_param schedParam;
        schedParam.sched_priority = sched_get_priority_max(SCHED_FIFO);  // 设置高优先级
        pthread_setschedparam(pthread_self(), SCHED_FIFO, &schedParam);          // 设置 FIFO 策略和优先级
#endif

        // 执行定时任务
        while (!tryToStop_) {
          std::this_thread::sleep_for(std::chrono::milliseconds(intervalMs));
          task();
        }
        stopInternal();
      }).detach();
    }
    void stop() {
      if (stopped_ || tryToStop_) {
        return;
      }

      tryToStop_ = true;
      waitForStop();
    }
    bool isStopped() const {
      return stopped_;
    }

   private:
    void stopInternal() {
      stopped_ = true;
      stopCond_.notify_one();
    }
    void waitForStop() {
      std::unique_lock<std::mutex> lck(mutex_);
      // 传递原子变量的引用到 lambda，避免捕获 this 指针
      stopCond_.wait(lck, [this]() { return this->stopped_.load(); });
      tryToStop_ = false;
    }

    std::atomic<bool> stopped_;
    std::atomic<bool> tryToStop_;
    std::mutex mutex_;
    std::condition_variable stopCond_;
  };

  using SubscriptionItemPtr = std::shared_ptr<SubscriptionItem>;

  bool subscribe(std::int32_t messageId, SubscriptionItemPtr item);
  bool unsubscribe(std::int32_t messageId, SubscriptionItemPtr item);
  void registerTimeoutCallback(SubscriptionItemPtr item);

  using CallbackMap = std::map<std::int32_t, std::vector<SubscriptionItemPtr>>;
  CallbackMap callbackMap_;

  std::vector<SubscriptionItemPtr> timeoutCallbackList_;
  std::mutex callbackMapMutex_;
  std::mutex timeoutCallbackListMutex_;
  PeriodicTaskScheduler taskScheduler_;  // 定时任务调度器对象
};

// 获取当前时间戳（单位：微秒）
static std::int64_t getTimeStamp() {
  return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch())
      .count();
}

// 使用 std::lock 避免死锁
void MessageBus::publishMessage(std::int32_t messageId, const std::vector<std::uint8_t>& messageContent,
                                std::int32_t additionalData) {
  // 使用 std::lock 锁住多个资源，避免死锁
  std::unique_lock<std::mutex> callbackMapLock(callbackMapMutex_, std::defer_lock);
  std::unique_lock<std::mutex> timeoutCallbackListLock(timeoutCallbackListMutex_, std::defer_lock);

  std::lock(callbackMapLock, timeoutCallbackListLock);  // 使用 std::lock 确保锁的顺序一致

  // 清除已超时的消息
  auto it = timeoutCallbackList_.begin();
  while (it != timeoutCallbackList_.end()) {
    if (std::find((*it)->subscribedMessageIds.begin(), (*it)->subscribedMessageIds.end(), messageId) !=
        (*it)->subscribedMessageIds.end()) {
      it = timeoutCallbackList_.erase(it);
    } else {
      ++it;
    }
  }

  auto callbackIt = callbackMap_.find(messageId);
  if (callbackIt == callbackMap_.end()) {
    return;
  }

  // 调用消息的回调函数
  for (auto& item : callbackIt->second) {
    if (item->messageCallback) {
      item->messageCallback(messageContent, additionalData);
    }
    if (item->subscriptionType == SubscriptionType::ONCE_SUBSCRIBE) {
      for (std::int32_t msgId : item->subscribedMessageIds) {
        unsubscribe(msgId, item);
      }
    }
  }
}

// 超时检查
void MessageBus::checkAndHandleTimeouts() {
  std::unique_lock<std::mutex> timeoutCallbackListlck(timeoutCallbackListMutex_);

  std::int64_t currentTime = getTimeStamp();
  for (auto it = timeoutCallbackList_.begin(); it != timeoutCallbackList_.end();) {
    if ((*it)->timeoutTimestampMicroseconds <= currentTime) {
      if ((*it)->timeoutCallback) {
        (*it)->timeoutCallback();
      }
      for (std::int32_t msgId : (*it)->subscribedMessageIds) {
        unsubscribe(msgId, *it);
      }
      it = timeoutCallbackList_.erase(it);
    } else {
      ++it;
    }
  }
}

void MessageBus::registerTimeoutCallback(SubscriptionItemPtr item) {
  item->timeoutTimestampMicroseconds = getTimeStamp() + item->timeoutIntervalMilliseconds * 1000;
  timeoutCallbackList_.push_back(item);
}

void MessageBus::clearAllSubscriptions() {
  std::unique_lock<std::mutex> callbackMaplck(callbackMapMutex_);
  std::unique_lock<std::mutex> timeoutCallbackListlck(timeoutCallbackListMutex_);
  callbackMap_.clear();
  timeoutCallbackList_.clear();
}

void MessageBus::start() {
  if (taskScheduler_.isStopped()) {
    taskScheduler_.startTask(DEFAULT_TIMER_INTERVAL_MS, std::bind(&MessageBus::checkAndHandleTimeouts, this));
  }
}

void MessageBus::stop() {
  taskScheduler_.stop();
}

bool MessageBus::subscribe(std::int32_t messageId, SubscriptionItemPtr item) {
  std::lock_guard<std::mutex> lck(callbackMapMutex_);
  auto it = callbackMap_.find(messageId);
  if (it != callbackMap_.end()) {
    it->second.push_back(item);
  } else {
    callbackMap_[messageId] = {item};
  }
  registerTimeoutCallback(item);
  return true;
}

bool MessageBus::unsubscribe(std::int32_t messageId, SubscriptionItemPtr item) {
  std::lock_guard<std::mutex> lck(callbackMapMutex_);
  auto it = callbackMap_.find(messageId);
  if (it != callbackMap_.end()) {
    it->second.erase(std::remove(it->second.begin(), it->second.end(), item), it->second.end());
  }
  return true;
}

bool MessageBus::subscribeToMessage(const SubscriptionItem& item) {
  auto itemPtr = std::make_shared<SubscriptionItem>(item);
  for (std::int32_t messageId : item.subscribedMessageIds) {
    if (!subscribe(messageId, itemPtr)) {
      return false;
    }
  }
  return true;
}

}  // namespace MessageBusSystem

#endif  // MESSAGE_BUS_H
