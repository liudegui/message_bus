﻿#include <atomic>
#include <chrono>
#include <functional>
#include <iostream>
#include <random>
#include <string>
#include <thread>
#include <vector>

#include "message_bus.hpp"

using namespace MessageBusSystem;

// 订阅回调函数
void messageCallback(const std::vector<std::uint8_t>& messageContent, std::int32_t additionalData) {
  // 这里模拟消息的处理过程
}

void timeoutCallback() {
  std::cout << "Timeout callback triggered!" << std::endl;
}

void singleThreadTest() {
  std::cout << "Running Single Thread Test..." << std::endl;
  MessageBus& bus = MessageBus::instance();

  // 创建一个订阅项
  SubscriptionItem item;
  item.messageCallback = messageCallback;
  item.timeoutCallback = timeoutCallback;
  item.timeoutIntervalMilliseconds = 1000;  // 1秒超时
  item.subscribedMessageIds.push_back(1);
  bus.subscribeToMessage(item);
  const std::string str{"Test message"};
  const std::vector<std::uint8_t> testMsg(str.begin(), str.end());

  // 开始消息总线
  bus.start();

  // 发布大量消息
  auto start = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < 100000; ++i) {
    bus.publishMessage(1, testMsg, i);
  }

  // 测量时间
  auto end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> duration = end - start;
  std::cout << "Single Thread Test Completed in " << duration.count() << " seconds." << std::endl;
  bus.stop();
}

void multiThreadTest() {
  std::cout << "Running Multi Thread Test..." << std::endl;
  MessageBus& bus = MessageBus::instance();

  // 创建订阅项
  SubscriptionItem item;
  item.messageCallback = messageCallback;
  item.timeoutCallback = timeoutCallback;
  item.timeoutIntervalMilliseconds = 500;  // 0.5秒超时
  item.subscribedMessageIds.push_back(1);
  bus.subscribeToMessage(item);
  const std::string str{"Test message"};
  const std::vector<std::uint8_t> testMsg(str.begin(), str.end());
  // 启动消息总线
  bus.start();

  // 发布消息的线程数
  const int numThreads = 8;
  std::vector<std::thread> threads;
  auto start = std::chrono::high_resolution_clock::now();

  // 多线程发布消息
  for (int i = 0; i < numThreads; ++i) {
    threads.emplace_back([&]() {
      for (int j = 0; j < 10000; ++j) {
        bus.publishMessage(1, testMsg, j);
      }
    });
  }

  // 等待所有线程完成
  for (auto& t : threads) {
    t.join();
  }

  // 测量时间
  auto end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> duration = end - start;
  std::cout << "Multi Thread Test Completed in " << duration.count() << " seconds." << std::endl;
  bus.stop();
}

void messageTimeoutTest() {
  std::cout << "Running Message Timeout Test..." << std::endl;
  MessageBus& bus = MessageBus::instance();

  // 创建订阅项，设置为超时回调
  SubscriptionItem item;
  item.messageCallback = messageCallback;
  item.timeoutCallback = timeoutCallback;
  item.timeoutIntervalMilliseconds = 2000;  // 2秒超时
  item.subscribedMessageIds.push_back(1);
  bus.subscribeToMessage(item);
  const std::string str{"Test message"};
  const std::vector<std::uint8_t> testMsg(str.begin(), str.end());
  // 启动消息总线
  bus.start();

  // 发布消息
  bus.publishMessage(1, testMsg, 0);

  // 等待超时回调
  std::this_thread::sleep_for(std::chrono::seconds(3));  // 等待超过超时

  // 停止消息总线
  bus.stop();
}

void highFrequencyTest() {
  std::cout << "Running High Frequency Test..." << std::endl;
  MessageBus& bus = MessageBus::instance();

  // 创建订阅项
  SubscriptionItem item;
  item.messageCallback = messageCallback;
  item.timeoutCallback = timeoutCallback;
  item.timeoutIntervalMilliseconds = 500;  // 0.5秒超时
  item.subscribedMessageIds.push_back(1);
  bus.subscribeToMessage(item);
  const std::string str{"High frequency message"};
  const std::vector<std::uint8_t> testMsg(str.begin(), str.end());
  // 启动消息总线
  bus.start();

  // 发布高频消息
  auto start = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < 100000; ++i) {
    bus.publishMessage(1, testMsg, i);
  }

  // 测量时间
  auto end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> duration = end - start;
  std::cout << "High Frequency Test Completed in " << duration.count() << " seconds." << std::endl;
  bus.stop();
}

int main() {
  // 单线程压力测试
  singleThreadTest();

  // 多线程压力测试
  multiThreadTest();

  // 消息超时测试
  messageTimeoutTest();

  // 高频消息测试
  highFrequencyTest();

  return 0;
}
